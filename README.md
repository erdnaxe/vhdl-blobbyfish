# VHDL Blobyfish

Blobyfish est un projet VHDL développé par un groupe de projet en SAPHIRE-EEA.

C'est un clone de Flappybird mais en VHDL.

## Structure du programme

![Structure entre les modules VHDL](schema.svg)

## TODO-list

  - Randomize pipe altitude,
  - BCD counter for score.
